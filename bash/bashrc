################################################################################
# General settings
################################################################################

if [ -f /opt/local/etc/profile.d/bash_completion.sh ]; then
    . /opt/local/etc/profile.d/bash_completion.sh
    source ~/bin/git-completion.bash
    source ~/bin/mercurial-completion.bash
fi

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

################################################################################
# History settings
################################################################################

# don't put duplicate lines in the history. See bash(1) for more options
# don't overwrite GNU Midnight Commander's setting of `ignorespace'.
export HISTCONTROL=$HISTCONTROL${HISTCONTROL+,}ignoredups
# ... or force ignoredups and ignorespace
export HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

################################################################################
# Prompt settings
################################################################################

# set a color prompt
case "$TERM" in
    *-*color) color_prompt=yes;;
esac

if [ "$color_prompt" = yes ]; then
    PS1='\[\033[01;31m\]\u@\h\[\033[00m\]:\[\033[01;36m\]\w\[\033[00m\]'
    PS1+='$(last_exit=$? ; if git branch > /dev/null 2> /dev/null ; '
    PS1+='then echo -n ".\[\033[01;33m\]$(git branch 2> /dev/null | '
    PS1+='grep "^*" | colrm 1 2)\[\033[00m\]" ; fi ; echo ".'
    PS1+='\[\033[01;31m\]{\[\033[00m\]$last_exit\[\033[01;31m\]}\[\033[00m\]\$") '
else
    PS1='\u@\h:\w\$ '
fi

# enable color support of ls and also add handy aliases
export CLICOLOR=1
export LSCOLORS=ExGxcxdxCxegedabagacad
